package main

import "github.com/gin-gonic/gin"

func ping(c *gin.Context) {
  c.JSON(200, gin.H{
    "message": "pong",
  })
}

func main() {
  r := gin.Default()

  r.StaticFile("/", "./public/index.html")
  r.StaticFile("/bundle.js", "./public/bundle.js")
  r.StaticFile("/bundle.js.map", "./public/bundle.js.map")
  r.StaticFile("/libs/react.min.js", "../node_modules/react/dist/react.min.js")
  r.StaticFile("/libs/react-dom.min.js", "../node_modules/react-dom/dist/react-dom.min.js")

  apiv1 := r.Group("/api/v1")
  {
    apiv1.GET("/ping", ping)
  }

  r.Run() // listen and serve on 0.0.0.0:8080
}
